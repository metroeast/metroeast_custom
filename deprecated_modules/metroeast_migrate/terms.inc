<?php

/**
 * @file
 * Migration classes for nodes
 */
class MetroEastTermMigration extends DrupalTerm7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

  }

  public function createStub(Migration $migration, array $source_id) {
    return FALSE;
  }
}

class MetroEastMultidayEventTypeTerm extends DrupalTerm7Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

  }

  public function createStub(Migration $migration, array $source_id) {
    return FALSE;
  }
}
