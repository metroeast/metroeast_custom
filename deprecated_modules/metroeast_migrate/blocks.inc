<?php

/**
 * @file
 * Block content migration
 */

class MetroEastBlockMigration extends DynamicMigration {
  public function __construct() {
    parent::__construct();

    $this->description = 'Custom Block table';
    $table_name = 'block_custom';
    $this->map = new MigrateSQLMap($this->machineName,
      array('bid' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
             )
           ),
        MigrateDestinationTable::getKeySchema($table_name)
      );
    $query = Database::getConnection('default', 'live')
         ->select($table_name, 'b')
             ->fields('b', array('bid', 'body', 'info', 'format'));
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationTable($table_name);
    // Mapped fields
    $this->addSimpleMappings(array('bid', 'body', 'info', 'format'));
  }
}
