<?php
/**
 * @file
 * metroeast_lph.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function metroeast_lph_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'landing_page_header';
  $bean_type->label = 'Landing Page Header';
  $bean_type->options = '';
  $bean_type->description = 'Header image and links for landing page such as /getreel';
  $export['landing_page_header'] = $bean_type;

  return $export;
}
