#!/bin/bash
#
# v1.1.0 -- IMU for MetroEast, elkram 2016, copyleft attribution, share and modify
#
#
#     see: telvue_thumb_vod.setup.sh
#


## subshell the server conversation / don't wait
# pull all metadata
# diff for updates
# curl for vod uri and thumbnail
# copy thumbnail to site folder
# make csv
(
  # flag that were busy
  touch "$script_path/all_content_metadata.pid"
  
  
  # are we continuing or make a new diff?
  if [ ! -f "$script_path/all_content_metadata.last" ]
  then
    ## we want fresh data
    continue_after=0
    
    ## dump raw content metadata file
    sleep $curl_throttle
    [ $test_logging -gt 0 ] && test_log "curl: http://$server_address/all_content_metadata"
    "$my_curl" -sL http://$server_address/all_content_metadata -o "$script_path/all_content_metadata"
    
    ## output data to keep
    # multi line grep for content: id, filename, updated-at
    "$my_pcregrep" -M '<content-file>(.|\n)*?<id |<content-files>|<updated-at |<filename>' "$script_path/all_content_metadata" > "$script_path/all_content_metadata.new"
    
    ## diff the previous against the new
    # grep for updated with before context, remove diff lines prefixed in: +++, @ and -
    touch "$script_path/all_content_metadata.prev"
    "$my_git" diff --patch "$script_path/all_content_metadata.prev" "$script_path/all_content_metadata.new" | grep -B 5 -E '\+ *<updated-at ' | grep -vE '^(-|@|\+\+\+)' > "$script_path/updates"
    
    # maintain a previous copy for comparison
    cp "$script_path/all_content_metadata.new" "$script_path/all_content_metadata.prev"
    
  else
    ## we want to continue after, integer in flag file
    continue_after=$(cat "$script_path/all_content_metadata.last")
  fi



  # reset the csv output with a header
  ## :> "$script_path/updates.csv"
  csv_line=$(csv_this <<CSV_ME
cid
content_file
content_img
content_vod
CSV_ME
)
  echo "$csv_line" > "$script_path/updates.csv"

  ## loop through updates
  # first parse the filename, does it match expected format: #####_####.ext
  # update filename in table, if marked + by diff
  # 
  # get VOD URL by id (presume need to update)
  process_count=0
  this_count=0

  while read this_update_line
  do
    case "$this_update_line" in
      ## the end of data
      '##END OF DATA##')
        rm "$script_path/all_content_metadata.last"
        ;;
        
      
      ## the first item for each content record
      *\<content-file\>*)
        this_id=
        this_filename=
        let "process_count++"
        [ $process_count -le $continue_after ] && continue;
        let "this_count++"
        ## this_filename_upd=1
        ;;
    
    
      # parse id
      *\<id' 'type*\>*)
        [ $process_count -le $continue_after ] && continue;
        this_id=${this_update_line#*>}
        this_id=${this_id%<*}
        [ $test_logging -gt 0 ] && test_log "id:  $this_id"
        ;;
    
    
      # parse and confim filename pattern
      *\<filename\>*)
        [ $process_count -le $continue_after ] && continue;
        this_filename=${this_update_line#*>}
        this_filename=${this_filename%<*}
        
        if [ "$this_filename" == "" ]
        then
          ## skipping, empty
          [ $test_logging -gt 0 ] && test_log "skipping empty filename"
          continue
        fi
      
        ## filename must match code/pattern
        # program number in 5 digits
        # episode number in 4 digits (optional) separated by an underscore
        #
        #   ppppp_eeee.ext
        #   ppppp.ext
        #
        test_filename=$(echo "$this_filename" | grep -oE '\d\d\d\d\d\.[a-z0-9]*|\d\d\d\d\d_\d\d\d\d\.[a-z0-9]*')
        if [ "$test_filename" != "$this_filename" ]
        then
          ## not a file we care about, skipping
          [ $test_logging -gt 0 ] && test_log "skipping file:  $this_filename"
          this_filename=
          continue
        fi
        this_filename=${this_filename%.*}
      
        ## is this an update? 
        ## this_filename_upd=$(echo "$this_update_line" | grep -qE '^\+'; echo $?)
      
        ##echo "file[${yn[$this_filename_upd]}]:  $this_filename"
        [ $test_logging -gt 0 ] && test_log "file:  $this_filename"
        ;;
    
    
      ## the last item for each content record, where we do the work
      *\<updated-at*\>*)
        [ $process_count -le $continue_after ] && continue;
        
        
        # filename matches format?
        if [ "$this_filename" == "" ]
        then
          # nothing to do next record
          continue
        fi
        
        # id present?
        if [ "$this_id" == "" ]
        then
          # nothing to do next record
          continue
        fi
        
      
        # get URL
        sleep $curl_throttle
        [ $test_logging -gt 1 ] && test_log "curl: http://$server_address/content_metadata/$this_id"
        "$my_curl" -sL http://$server_address/content_metadata/$this_id -o "$script_path/content_metadata"
        this_vodURL=$(fgrep vodURL "$script_path/content_metadata")
        this_vodURL=${this_vodURL#*>}
        this_vodURL=${this_vodURL%<*}
        [ $test_logging -gt 1 ] && test_log "vodURL:  $this_vodURL"
      
        # get image
        this_image=
        sleep $curl_throttle
        [ $test_logging -gt 1 ] && test_log "curl: http://$server_address/thumbnails/$this_id.jpg"
        "$my_curl" -sL http://$server_address/thumbnails/$this_id.jpg -o "$script_path/content_image"
        # confirm we have a file and not html/file not found
        fgrep -q 'DOCTYPE HTML PUBLIC' "$script_path/content_image" && fgrep -q 'File not found.' "$script_path/content_image"
        if [ $? -ne 0 ]
        then
          # file is probably good
          this_image=$this_id.jpg
          [ $test_logging -gt 0 ] && test_log "copy: $images_dir/$this_image"
          cp "$script_path/content_image" "$images_dir/$this_image"
        else
          # no image returned
          [ $test_logging -gt 1 ] && test_log "skipping copy, no image"
        fi
      
        ## testing
        #echo "id:  $this_id"
        #echo "file[${yn[$this_filename_upd]}]:  $this_filename"
        #echo "URL:  $this_vodURL"
        #echo "img:  $this_image"
        #[ "$this_image" != "" ] && open $this_image
        
        csv_line=$(csv_this <<CSV_ME
$this_id
$this_filename
$this_image
$this_vodURL
CSV_ME
)
        [ $test_logging -gt 1 ] && test_log "$csv_line"
        echo "$csv_line" >> "$script_path/updates.csv"
        
        # save placeholder, make flag to continue and end loop
        [ $test_logging -gt 0 ] && test_log "completed # $process_count"
        echo "$process_count" > "$script_path/all_content_metadata.last"
        
        ## Have we reached the limit?
        if [ $this_count -ge $update_limit ]
        then
          [ $test_logging -gt 0 ] && test_log "update limit reached, stopping"
          break
        fi
      
        ;;
    
    esac
  done < <(
    cat "$script_path/updates"
    echo "##END OF DATA##"
  )


  ## flag update available
  touch "$script_path/all_content_metadata.update"

  ## remove busy flag
  rm "$script_path/all_content_metadata.pid"

) &
## do not wait...
