#!/bin/bash
#
# v1.1.0 -- IMU for MetroEast, elkram 2016, copyleft attribution, share and modify
#
#
# script to sync images for TelVue content and provide CSV arrays for table updates
# works with PHP scripting which executes the .status script
# when UPDATE is indicated (CSV is ready), the PHP performs a SQL merge
# table setup is in the PHP script
# when READY is indicated, time to run the background metadata curl script: .bgsync
#
# exit  stdout  status
#
#  0    RECENT  the all_content_metadata on file is too recent, skips further execution
#  1    MAKE    message to PHP script to call the .bgsync script
# 86    BUSY    pid file found, assume already executing, skips further execution
# 22    UPDATE  CSV file is ready, signal to PHP, skips further execution
########### 42    MAKING  the process is underway in a subshell
#
#
# files we make/use
#
# all_content_metadata          recent raw output from TelVue server REST API
# all_content_metadata.new      recent filtered copy of all_content_metadata
# all_content_metadata.prev     previous copy of all_content_metadata.new for diff
# all_content_metadata.pid      flag file: working copy lock
# all_content_metadata.update   flag file: CSV update waiting for import/merge
# all_content_metadata.last     flag file with integer: last item we successfully processed from updates
# updates                       filter diff result vew vs. prev, all records we want to update
# updates.csv                   CSV data for import/merge
# all_content.log               setup test log
# content_metadata              recent raw output from content_metadata/#ID#
# content_image                 rceent raw output from thumbnails/#ID#.jpg
#
#
# this setup file is shared by:
#   telvue_thumb_vod.status.sh
#   telvue_thumb_vod.bgsync.sh
#
#


## setup, should be 0 for production sites
test_logging=2

# telvue content server address/domain
#server_address=192.168.1.183
server_address=hc1.mctv.org

# site image path
images_dir="/Volumes/Storage/Sites/dev3.metroeast.org/sites/default/files/pg-img"

# script path
script_path=$(dirname "$0")

# how often we check for updates (thrice a day is plenty)
check_hours=8

# sec delay before each curl
curl_throttle=.8

# update limit (max number of rows returned for each batch)
update_limit=100

# where are the utilities
my_git=/usr/local/bin/git
my_pcregrep=/usr/local/bin/pcregrep
my_curl=curl

# y/n
yn[0]=yes
yn[1]=no




## functions


# output CSV from read/stdin pipe
function csv_this ()
{
  ## rolls each row or read input into a column, delimited by ,
  #    c = csv (wrapped in ", includes " escaping)
  #
  
  return_LINE=''
  glue_char=','

  ## read each column as a line from stdin/pipe/heredoc
  glue=''
  IFS=$'\n'
  while read this_data
  do
    # escape " as "", and wrap in "
    this_data="\"${this_data//\"/\"\"}\""
    
    # accum the line
    return_LINE="$return_LINE$glue$this_data"
    
    # set glue char
    glue="$glue_char"
  done
  
  echo "$return_LINE"
}


function test_log ()
{
  ## local log of activity for setup testing
  #
  now=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$now :: $1" >> "$script_path/all_content.log"
}

#### END ####
