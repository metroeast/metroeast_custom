#!/bin/bash
#
# v1.1.0 -- IMU for MetroEast, elkram 2016, copyleft attribution, share and modify
#
#
#     see: telvue_thumb_vod.setup.sh
#

## discovered that an empty (head only in the csv) update file will still signal an update needed
## should adjust this to prevent the unnecessary CSV parsing PHP routine execution time



## multiple instance management and status response
#

# already running?
if [ -f "$script_path/all_content_metadata.pid" ]
then
  [ $test_logging -gt 0 ] && test_log "BUSY -> PHP call, found pid file"
  echo "BUSY"
  exit 86
fi

# update waiting?
if [ -f "$script_path/all_content_metadata.update" ]
then
  [ $test_logging -gt 0 ] && test_log "UPDATE -> PHP call, awaiting parsing"
  echo "UPDATE"
  exit 22
fi

# old enough?
test=$(find "$script_path/all_content_metadata" -mtime -${check_hours}h)
if [ "$test" != "" ]
then
  [ $test_logging -gt 0 ] && test_log "RECENT -> PHP call, less than $check_hours hours ago"
  echo "RECENT"
  exit 0
fi

# we're ready to make new data
[ $test_logging -gt 0 ] && test_log "MAKE -> PHP call, signal to launch bg process"
echo "MAKE"
exit 1

# status report for calling agent
#[ $test_logging -gt 0 ] && test_log "MAKING -> PHP call, bg process launched"
#echo "MAKING"
#exit 42

