<?php
/**
 * @file
 * metroeast_image.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function metroeast_image_image_default_styles() {
  $styles = array();

  // Exported image style: circle_116.
  $styles['circle_116'] = array(
    'label' => 'Circle 116',
    'effects' => array(
      20 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 128,
          'height' => 128,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => -10,
      ),
      21 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 116,
          'height' => 116,
          'anchor' => 'center-center',
        ),
        'weight' => -9,
      ),
      23 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => -8,
      ),
      22 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 58,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  // Exported image style: featured_media.
  $styles['featured_media'] = array(
    'label' => 'Featured Media (840x540)',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 840,
          'height' => 540,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: featured_media__42_width__280x180_.
  $styles['featured_media__42_width__280x180_'] = array(
    'label' => 'Featured Media .42 width (280x180)',
    'effects' => array(
      18 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 180,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: featured_media_article.
  $styles['featured_media_article'] = array(
    'label' => 'Featured Media Article (352)',
    'effects' => array(
      17 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 352,
          'height' => 352,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: featured_media_half_width__336x216_.
  $styles['featured_media_half_width__336x216_'] = array(
    'label' => 'Featured Media .5 half width (336x216)',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 336,
          'height' => 216,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: featured_media_large.
  $styles['featured_media_large'] = array(
    'label' => 'Featured Media Large (1680x1080)',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1680,
          'height' => 1080,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: featured_media_quarter_width__168x108_.
  $styles['featured_media_quarter_width__168x108_'] = array(
    'label' => 'Featured Media .25 quarter width (168x108)',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 168,
          'height' => 108,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: featured_media_slide_front.
  $styles['featured_media_slide_front'] = array(
    'label' => 'Featured Media Slide Front Page (754x342)',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 754,
          'height' => 382,
        ),
        'weight' => 0,
      ),
      8 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 754,
          'height' => 342,
          'anchor' => 'center-bottom',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: featured_media_small.
  $styles['featured_media_small'] = array(
    'label' => 'Featured Media .33 third width (210x135)',
    'effects' => array(
      19 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 210,
          'height' => 135,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: featured_media_small_halfvert__210x65_.
  $styles['featured_media_small_halfvert__210x65_'] = array(
    'label' => 'Featured Media .33 Small halfvert (210x65)',
    'effects' => array(
      15 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 186,
          'height' => 72,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
      16 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 168,
          'height' => 64,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: featured_media_thumb.
  $styles['featured_media_thumb'] = array(
    'label' => 'Featured Media .17 Thumbnail (105x76)',
    'effects' => array(
      9 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 117,
          'height' => 84,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
      10 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 105,
          'height' => 76,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: featured_media_tiny.
  $styles['featured_media_tiny'] = array(
    'label' => 'Featured Media .08 twelfth width (52x42)',
    'effects' => array(
      13 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 62,
          'height' => 50,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
      14 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 52,
          'height' => 42,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: media_thumbnail.
  $styles['media_thumbnail'] = array(
    'label' => 'Media thumbnail (100x80)',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 80,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: sub_header.
  $styles['sub_header'] = array(
    'label' => 'Sub Header (747x93)',
    'effects' => array(
      0 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 747,
          'height' => 93,
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
