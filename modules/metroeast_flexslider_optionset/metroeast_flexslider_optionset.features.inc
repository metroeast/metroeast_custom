<?php
/**
 * @file
 * metroeast_flexslider_optionset.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function metroeast_flexslider_optionset_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}
