<?php
/**
 * @file
 * metroeast_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function metroeast_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-get-reel:/content/about-get-reel
  $menu_links['main-menu_about-get-reel:/content/about-get-reel'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/about-get-reel',
    'router_path' => '',
    'link_title' => 'About Get Reel',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_about-get-reel:/content/about-get-reel',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_get-reel:getreel',
  );
  // Exported menu link: main-menu_become-a-partner:node/838
  $menu_links['main-menu_become-a-partner:node/838'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/838',
    'router_path' => 'node/%',
    'link_title' => 'Become a Partner',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_become-a-partner:node/838',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_partner-with-us:partnerwithus',
  );
  // Exported menu link: main-menu_become-a-producer:/content/become-producer
  $menu_links['main-menu_become-a-producer:/content/become-producer'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/become-producer',
    'router_path' => '',
    'link_title' => 'Become A Producer',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_become-a-producer:/content/become-producer',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: main-menu_board:node/452
  $menu_links['main-menu_board:node/452'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/452',
    'router_path' => 'node/%',
    'link_title' => 'Board',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_board:node/452',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-us:node/449',
  );
  // Exported menu link: main-menu_checkout-gear:/content/check-out-gear-now
  $menu_links['main-menu_checkout-gear:/content/check-out-gear-now'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/check-out-gear-now',
    'router_path' => '',
    'link_title' => 'Checkout Gear',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_checkout-gear:/content/check-out-gear-now',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: main-menu_contact-us:node/449
  $menu_links['main-menu_contact-us:node/449'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/449',
    'router_path' => 'node/%',
    'link_title' => 'Contact Us',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_contact-us:node/449',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-us:node/449',
  );
  // Exported menu link: main-menu_fast-track:/getreel/
  $menu_links['main-menu_fast-track:/getreel/'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/getreel/',
    'router_path' => '',
    'link_title' => 'Fast Track',
    'options' => array(
      'fragment' => 'fasttrack',
      'attributes' => array(),
      'identifier' => 'main-menu_fast-track:/getreel/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_get-reel:getreel',
  );
  // Exported menu link: main-menu_get-reel:getreel
  $menu_links['main-menu_get-reel:getreel'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'getreel',
    'router_path' => 'getreel',
    'link_title' => 'Get Reel',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_get-reel:getreel',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: main-menu_hire-us:/content/hire-us-create-your-production
  $menu_links['main-menu_hire-us:/content/hire-us-create-your-production'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/hire-us-create-your-production',
    'router_path' => '',
    'link_title' => 'Hire Us',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_hire-us:/content/hire-us-create-your-production',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_how-it-works:node/837
  $menu_links['main-menu_how-it-works:node/837'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/837',
    'router_path' => 'node/%',
    'link_title' => 'How It Works',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_how-it-works:node/837',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_partner-with-us:partnerwithus',
  );
  // Exported menu link: main-menu_make-a-show:makeashow
  $menu_links['main-menu_make-a-show:makeashow'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'makeashow',
    'router_path' => 'makeashow',
    'link_title' => 'Make A Show',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_make-a-show:makeashow',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_metroeast-partners:/partnerwithus/recent
  $menu_links['main-menu_metroeast-partners:/partnerwithus/recent'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/partnerwithus/recent',
    'router_path' => '',
    'link_title' => 'MetroEast Partners',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_metroeast-partners:/partnerwithus/recent',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_partner-with-us:partnerwithus',
  );
  // Exported menu link: main-menu_partner-with-us:partnerwithus
  $menu_links['main-menu_partner-with-us:partnerwithus'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'partnerwithus',
    'router_path' => 'partnerwithus',
    'link_title' => 'Partner With Us',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_partner-with-us:partnerwithus',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 1,
    'customized' => 1,
  );
  // Exported menu link: main-menu_partnerships:/getreel/recent/partnership
  $menu_links['main-menu_partnerships:/getreel/recent/partnership'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/getreel/recent/partnership',
    'router_path' => '',
    'link_title' => 'Partnerships',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_partnerships:/getreel/recent/partnership',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_get-reel:getreel',
  );
  // Exported menu link: main-menu_sign-up:https://docs.google.com/a/metroeast.org/forms/d/1g9r4I1zJAxN3d1172hqxwVUHqYUohWGliqbHXiGJdH0/viewform?embedded=true&width=600&height=500&iframe=true
  $menu_links['main-menu_sign-up:https://docs.google.com/a/metroeast.org/forms/d/1g9r4I1zJAxN3d1172hqxwVUHqYUohWGliqbHXiGJdH0/viewform?embedded=true&width=600&height=500&iframe=true'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'https://docs.google.com/a/metroeast.org/forms/d/1g9r4I1zJAxN3d1172hqxwVUHqYUohWGliqbHXiGJdH0/viewform?embedded=true&width=600&height=500&iframe=true',
    'router_path' => '',
    'link_title' => 'Sign Up',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'colorbox-load',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_sign-up:https://docs.google.com/a/metroeast.org/forms/d/1g9r4I1zJAxN3d1172hqxwVUHqYUohWGliqbHXiGJdH0/viewform?embedded=true&width=600&height=500&iframe=true',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_get-reel:getreel',
  );
  // Exported menu link: main-menu_submit-show:/content/how-submit-your-show
  $menu_links['main-menu_submit-show:/content/how-submit-your-show'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/how-submit-your-show',
    'router_path' => '',
    'link_title' => 'Submit Show',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_submit-show:/content/how-submit-your-show',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: main-menu_training--workshops:/content/training-workshops
  $menu_links['main-menu_training--workshops:/content/training-workshops'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/content/training-workshops',
    'router_path' => '',
    'link_title' => 'Training & Workshops',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_training--workshops:/content/training-workshops',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: main-menu_upcoming-events:events
  $menu_links['main-menu_upcoming-events:events'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'events',
    'router_path' => 'events',
    'link_title' => 'Upcoming Events',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_upcoming-events:events',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_partner-with-us:partnerwithus',
  );
  // Exported menu link: main-menu_videos:/getreel/gallery/vid
  $menu_links['main-menu_videos:/getreel/gallery/vid'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/getreel/gallery/vid',
    'router_path' => '',
    'link_title' => 'Videos',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_videos:/getreel/gallery/vid',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_get-reel:getreel',
  );
  // Exported menu link: main-menu_youth-programs:/getreel
  $menu_links['main-menu_youth-programs:/getreel'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '/getreel',
    'router_path' => '',
    'link_title' => 'Youth Programs',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_youth-programs:/getreel',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_make-a-show:makeashow',
  );
  // Exported menu link: menu-quicklinks_hours:node/296
  $menu_links['menu-quicklinks_hours:node/296'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => 'node/296',
    'router_path' => 'node/%',
    'link_title' => 'Hours',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-quicklinks_hours:node/296',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-quicklinks_pay-activity-fee:civicrm/contribute/transact
  $menu_links['menu-quicklinks_pay-activity-fee:civicrm/contribute/transact'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => 'civicrm/contribute/transact',
    'router_path' => 'civicrm',
    'link_title' => 'Pay Activity Fee',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'query' => array(
        'reset' => 1,
        'id' => 1,
      ),
      'identifier' => 'menu-quicklinks_pay-activity-fee:civicrm/contribute/transact',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-quicklinks_pay-all-class-pass-fee:civicrm/contribute/transact
  $menu_links['menu-quicklinks_pay-all-class-pass-fee:civicrm/contribute/transact'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => 'civicrm/contribute/transact',
    'router_path' => 'civicrm',
    'link_title' => 'Pay All-Class-Pass Fee',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'query' => array(
        'reset' => 1,
        'id' => 2,
      ),
      'identifier' => 'menu-quicklinks_pay-all-class-pass-fee:civicrm/contribute/transact',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-quicklinks_sign-up-for-a-workshop:/events/calendar
  $menu_links['menu-quicklinks_sign-up-for-a-workshop:/events/calendar'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => '/events/calendar',
    'router_path' => '',
    'link_title' => 'Sign-up for a Workshop',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-quicklinks_sign-up-for-a-workshop:/events/calendar',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-quicklinks_submit-a-program:node/693
  $menu_links['menu-quicklinks_submit-a-program:node/693'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => 'node/693',
    'router_path' => 'node/%',
    'link_title' => 'Submit a program',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-quicklinks_submit-a-program:node/693',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-quicklinks_whats-playing-now:/guide
  $menu_links['menu-quicklinks_whats-playing-now:/guide'] = array(
    'menu_name' => 'menu-quicklinks',
    'link_path' => '/guide',
    'router_path' => '',
    'link_title' => 'What\'s playing now?',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(),
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-quicklinks_whats-playing-now:/guide',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: user-menu_log-out:user/logout
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: user-menu_user-account:user
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: user-menu_volunteer-producer-login:/user/login
  $menu_links['user-menu_volunteer-producer-login:/user/login'] = array(
    'menu_name' => 'user-menu',
    'link_path' => '/user/login',
    'router_path' => '',
    'link_title' => 'Volunteer Producer Login',
    'options' => array(
      'roles_for_menu' => array(
        'show' => array(),
        'hide' => array(
          2 => 2,
        ),
      ),
      'attributes' => array(
        'id' => 'vlogin',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'user-menu_volunteer-producer-login:/user/login',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About Get Reel');
  t('Become A Producer');
  t('Become a Partner');
  t('Board');
  t('Checkout Gear');
  t('Contact Us');
  t('Fast Track');
  t('Get Reel');
  t('Hire Us');
  t('Home');
  t('Hours');
  t('How It Works');
  t('Log out');
  t('Make A Show');
  t('MetroEast Partners');
  t('Partner With Us');
  t('Partnerships');
  t('Pay Activity Fee');
  t('Pay All-Class-Pass Fee');
  t('Sign Up');
  t('Sign-up for a Workshop');
  t('Submit Show');
  t('Submit a program');
  t('Training & Workshops');
  t('Upcoming Events');
  t('User account');
  t('Videos');
  t('Volunteer Producer Login');
  t('What\'s playing now?');
  t('Youth Programs');


  return $menu_links;
}
